#!/bin/bash

nome=$1

while true; do
	echo "opcao (1) verificar exustencia do  user"
	echo "opcao (2) usuario logado"
	echo "opcao (3) listar arquivos do usuario"
	echo -e "opcao (4) sair\n"
	
	read -p "digite a opcao desejada: " op

	case $op in
		1) id $nome || echo "Usuario $nome não existe";;
		2) who | grep $nome || break;;
		3) ls /home/$nome || break;;
		4) break;;
	esac
done

echo "Usuario $nome não existe"
